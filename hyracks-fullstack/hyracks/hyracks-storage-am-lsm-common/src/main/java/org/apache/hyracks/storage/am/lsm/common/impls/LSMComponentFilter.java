/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.apache.hyracks.storage.am.lsm.common.impls;

import java.nio.ByteBuffer;

import org.apache.hyracks.api.dataflow.value.IBinaryComparatorFactory;
import org.apache.hyracks.api.dataflow.value.ISerializerDeserializer;
import org.apache.hyracks.api.exceptions.HyracksDataException;
import org.apache.hyracks.dataflow.common.data.accessors.ITupleReference;
import org.apache.hyracks.dataflow.common.data.marshalling.Integer64SerializerDeserializer;
import org.apache.hyracks.dataflow.common.data.marshalling.IntegerSerializerDeserializer;
import org.apache.hyracks.dataflow.common.util.TupleUtils;
import org.apache.hyracks.storage.am.common.api.ITreeIndexTupleReference;
import org.apache.hyracks.storage.am.common.api.ITreeIndexTupleWriter;
import org.apache.hyracks.storage.am.common.ophelpers.MultiComparator;
import org.apache.hyracks.storage.am.lsm.common.api.ILSMComponentFilter;

public class LSMComponentFilter implements ILSMComponentFilter {

    private final IBinaryComparatorFactory[] filterCmpFactories;
    private final ITreeIndexTupleWriter tupleWriter;

    private ITupleReference minTuple;
    private ITupleReference maxTuple;

    private byte[] minTupleBytes;
    private ByteBuffer minTupleBuf;

    private byte[] maxTupleBytes;
    private ByteBuffer maxTupleBuf;

    public LSMComponentFilter(ITreeIndexTupleWriter tupleWriter, IBinaryComparatorFactory[] filterCmpFactories) {
        this.filterCmpFactories = filterCmpFactories;
        this.tupleWriter = tupleWriter;
    }

    @Override
    public IBinaryComparatorFactory[] getFilterCmpFactories() {
        return filterCmpFactories;
    }

    @Override
    public void reset() {
        minTuple = null;
        maxTuple = null;
        minTupleBytes = null;
        maxTupleBytes = null;
        minTupleBuf = null;
        maxTupleBuf = null;
    }

    @Override
    public void update(ITupleReference tuple, MultiComparator cmp) throws HyracksDataException {
        if (minTuple == null) {
            int numBytes = tupleWriter.bytesRequired(tuple);
            minTupleBytes = new byte[numBytes];
            tupleWriter.writeTuple(tuple, minTupleBytes, 0);
            minTupleBuf = ByteBuffer.wrap(minTupleBytes);
            minTuple = tupleWriter.createTupleReference();
            ((ITreeIndexTupleReference) minTuple).resetByTupleOffset(minTupleBuf, 0);
        } else {

            int fieldcount = tuple.getFieldCount();
            int numBytes = 0;
            int[] numByte = new int[fieldcount];

            boolean[] ifFieldChanged = new boolean[fieldcount];
            boolean ifAnyFieldChanged = false;



            for(int i = 0; i< cmp.getComparators().length && i<fieldcount; i++)
            {
                ifFieldChanged[i]=false;
                int c = cmp.fieldRangeCompare(tuple, minTuple, i, 1);

                if (c < 0) {
                    ifFieldChanged[i]=true;
                    numByte[i] = tupleWriter.bytesRequiredSingleField(tuple,i,fieldcount);
                    numBytes+= numByte[i];
                    numByte[i] = numBytes;

                    ifAnyFieldChanged = true;
                }
                else
                {
                    numByte[i] += tupleWriter.bytesRequiredSingleField(minTuple,i,fieldcount);
                    numBytes+= numByte[i];
                    numByte[i] = numBytes;
                }
            }

            if(ifAnyFieldChanged) {

                if (minTupleBytes.length < numBytes) {
                    minTupleBytes = new byte[numBytes];
                }


                for (int i = 0; i < fieldcount; i++) {

                    int tupleoffset = i == 0 ? 0 : numByte[i - 1];

                    if(ifFieldChanged[i]) {
                        tupleWriter.writeTupleSingleField(tuple, i, fieldcount, minTupleBytes, tupleoffset);
                    }
                    else
                    {
                        tupleWriter.writeTupleSingleField(minTuple, i, fieldcount, minTupleBytes, tupleoffset);
                    }
                }

                minTupleBuf = ByteBuffer.wrap(minTupleBytes);
                ((ITreeIndexTupleReference) minTuple).resetByTupleOffset(minTupleBuf, 0);

            }


        }
        if (maxTuple == null) {
            int numBytes = tupleWriter.bytesRequired(tuple);
            maxTupleBytes = new byte[numBytes];
            tupleWriter.writeTuple(tuple, maxTupleBytes, 0);
            maxTupleBuf = ByteBuffer.wrap(maxTupleBytes);
            maxTuple = tupleWriter.createTupleReference();
            ((ITreeIndexTupleReference) maxTuple).resetByTupleOffset(maxTupleBuf, 0);
        } else {

            int fieldcount = tuple.getFieldCount();
            int numBytes = 0;
            int[] numByte = new int[fieldcount];

            boolean[] ifFieldChanged = new boolean[fieldcount];
            boolean ifAnyFieldChanged = false;
            for(int i = 0; i< cmp.getComparators().length && i<fieldcount; i++)
            {
                ifFieldChanged[i]=false;
                int c = cmp.fieldRangeCompare(tuple, maxTuple, i, 1);

                if (c > 0) {
                    ifFieldChanged[i]=true;
                    numByte[i] =  tupleWriter.bytesRequiredSingleField(tuple,i,fieldcount);
                    numBytes+= numByte[i];
                    numByte[i] = numBytes;
                    ifAnyFieldChanged = true;
                }
                else
                {
                    numByte[i] +=  tupleWriter.bytesRequiredSingleField(maxTuple,i,fieldcount);
                    numBytes+= numByte[i];
                    numByte[i] = numBytes;
                }
            }

            if(ifAnyFieldChanged) {

                if (maxTupleBytes.length < numBytes) {
                    maxTupleBytes = new byte[numBytes];
                }

                for (int i = 0; i < fieldcount; i++) {

                    int tupleoffset = (i == 0 ? 0 : numByte[i - 1]);
                    if(ifFieldChanged[i])
                    {
                        tupleWriter.writeTupleSingleField(tuple, i, fieldcount, maxTupleBytes, tupleoffset);
                    }
                    else
                    {
                        tupleWriter.writeTupleSingleField(maxTuple, i, fieldcount, maxTupleBytes, tupleoffset);
                    }
                }

                maxTupleBuf = ByteBuffer.wrap(maxTupleBytes);
                ((ITreeIndexTupleReference) maxTuple).resetByTupleOffset(maxTupleBuf, 0);

            }

        }
    }

    @Override
    public ITupleReference getMinTuple() {
        return minTuple;
    }

    @Override
    public ITupleReference getMaxTuple() {
        return maxTuple;
    }

    @Override
    public boolean satisfy(ITupleReference minTuple, ITupleReference maxTuple, MultiComparator filterCmp)
            throws HyracksDataException {

       // boolean isSatisfy = true;
//
//        ISerializerDeserializer[] fieldSerdes = new ISerializerDeserializer[] { Integer64SerializerDeserializer.INSTANCE,
//                IntegerSerializerDeserializer.INSTANCE};
//
//        System.out.println("Printing MinTuple");
//        System.out.println(TupleUtils.printTuple(this.minTuple,fieldSerdes));
//
//        System.out.println("Printing MaxTuple");
//        System.out.println(TupleUtils.printTuple(this.maxTuple,fieldSerdes));

        if (maxTuple != null && this.minTuple != null) {

            for(int i = 0; i< filterCmp.getComparators().length && i< maxTuple.getFieldCount(); i++)
            {
                int c = filterCmp.fieldRangeCompare(maxTuple, this.minTuple, i, 1);

                if (c < 0 ) {

                   // System.out.println("Pruning for this minTuple");
                    return false;
                }

            }


        }

        if (minTuple != null && this.maxTuple != null) {
            for(int i = 0; i< filterCmp.getComparators().length && i< minTuple.getFieldCount(); i++)
            {
                int c = filterCmp.fieldRangeCompare(minTuple, this.maxTuple, i , 1);
                if (c > 0) {
                   // System.out.println("Pruning for this maxTuple");

                    return false;
                }
            }

        }
        //System.out.println("Not Pruning");

        return true;
    }

}
